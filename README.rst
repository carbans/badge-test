Hola
=======

.. image:: https://img.shields.io/pypi/v/jabu.svg?style=flat-square
  :target: https://pypi.org/project/jabu/
  :alt: Latest PyPI version

.. image:: https://img.shields.io/pypi/pyversions/jabu.svg?style=flat-square
  :target: https://pypi.org/project/jabu/
  :alt: Python versions

Hay que instalar la siguiente dependencia::

	sudo apt install hola

Prueba con root::
	
	# apt install hola
